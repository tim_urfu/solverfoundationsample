﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolverFoundationSample
{
    class SolverRow
    {
        public int xId { get; set; }

        public double Koef { get; set; }

        public double Min { get; set; }

        public double Max { get; set; }

        public int KoefNerav1 { get; set; }

        public int KoefNerav2 { get; set; }
    }
}
