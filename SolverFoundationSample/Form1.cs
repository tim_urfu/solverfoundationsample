﻿using Microsoft.SolverFoundation.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SolverFoundationSample
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button_Calc_Click(object sender, EventArgs e)
        {
            List<SolverRow> solverList = new List<SolverRow>();

            // Исходные данные задачи
            solverList.Add(new SolverRow { xId = 1, Koef = 2, Min = 0, Max = 10, KoefNerav1 = 1, KoefNerav2 = 2 });
            solverList.Add(new SolverRow { xId = 2, Koef = -3, Min = 0, Max = 100, KoefNerav1 = 2, KoefNerav2 = 10 });
            solverList.Add(new SolverRow { xId = 3, Koef = 4, Min = 0, Max = 20, KoefNerav1 = 3, KoefNerav2 = -5 });

            SolverContext context = SolverContext.GetContext();
            Model model = context.CreateModel();

            Set users = new Set(Domain.Any, "users");

            Parameter k = new Parameter(Domain.Real, "Koef", users);
            k.SetBinding(solverList, "Koef", "xId");

            Parameter min = new Parameter(Domain.Real, "Min", users);
            min.SetBinding(solverList, "Min", "xId");

            Parameter max = new Parameter(Domain.Real, "Max", users);
            max.SetBinding(solverList, "Max", "xId");

            Parameter KoefNerav1 = new Parameter(Domain.Real, "KoefNerav1", users);
            KoefNerav1.SetBinding(solverList, "KoefNerav1", "xId");

            Parameter KoefNerav2 = new Parameter(Domain.Real, "KoefNerav2", users);
            KoefNerav2.SetBinding(solverList, "KoefNerav2", "xId");

            model.AddParameters(k, min, max, KoefNerav1, KoefNerav2);

            Decision choose = new Decision(Domain.RealNonnegative, "choose", users);
            model.AddDecision(choose);

            model.AddGoal("goal", GoalKind.Minimize, Model.Sum(Model.ForEach(users, xId => choose[xId] * k[xId])));

            // Ограничения-неравенства
            model.AddConstraint("Nerav1", Model.Sum(Model.ForEach(users, xId => KoefNerav1[xId] * choose[xId])) <= 900);
            model.AddConstraint("Nerav2", Model.Sum(Model.ForEach(users, xId => KoefNerav2[xId] * choose[xId])) <= 950);

            model.AddConstraint("c_choose", Model.ForEach(users, xId => (min[xId] <= choose[xId] <= max[xId])));

            try
            {
                Solution solution = context.Solve();
                Report report = solution.GetReport();

                String reportStr = "";

                for (int i = 0; i < solverList.Count; i++)
                {
                    reportStr += "Значение X" + (i+1).ToString() + ": " + choose.GetDouble(solverList[i].xId) + "\n";
                }
                reportStr += "\n" + report.ToString();

                MessageBox.Show(reportStr);
            }
            catch(Exception ex)
            {
                MessageBox.Show("При решении задачи оптимизации возникла исключительная ситуация.");
            }
        }
    }
}
